package biemann.android.badoochallenge.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import biemann.android.badoochallenge.R;

/**
 * Typical Viewholder pattern for RecyclerView
 */
public class ViewHolderTransactions extends RecyclerView.ViewHolder
{
    public LinearLayout mLinearLayoutContainer;
    public TextView mTextViewTransactionName;
    public TextView mTextViewTransactionPriceGbp;

    public ViewHolderTransactions(View itemView)
    {
        super(itemView);
        mLinearLayoutContainer = (LinearLayout) itemView.findViewById(R.id.transaction_layout_container);
        mTextViewTransactionName = (TextView) itemView.findViewById(R.id.transaction_name);
        mTextViewTransactionPriceGbp = (TextView) itemView.findViewById(R.id.transaction_converted_gbp);
    }

    @Override
    public String toString()
    {
        return super.toString() + " '" + mTextViewTransactionName.getText() + "'";
    }
}


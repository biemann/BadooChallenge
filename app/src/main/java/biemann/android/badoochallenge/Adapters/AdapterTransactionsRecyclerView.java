package biemann.android.badoochallenge.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import biemann.android.badoochallenge.ActivityTransactions;
import biemann.android.badoochallenge.Models.ItemModelProduct;
import biemann.android.badoochallenge.Models.ItemModelTransaction;
import biemann.android.badoochallenge.R;


public class AdapterTransactionsRecyclerView extends RecyclerView.Adapter<ViewHolderTransactions>
{
    private AppCompatActivity mAppCompatActivity;
    ArrayList<ItemModelTransaction> mTransactionsList;

    // Constructor
    public AdapterTransactionsRecyclerView(AppCompatActivity compatActivity, ArrayList<ItemModelTransaction> transactionsList)
    {
        mAppCompatActivity = compatActivity;
        mTransactionsList = transactionsList;
    }

    public void setNewData(ArrayList<ItemModelTransaction> transactionsList)
    {
        mTransactionsList = transactionsList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderTransactions onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View inflatedView = LayoutInflater.from(mAppCompatActivity).inflate(R.layout.row_transaction_item, viewGroup, false);
        return new ViewHolderTransactions(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolderTransactions viewHolder, int position)
    {
        final ItemModelTransaction transaction = getItem(position);
        viewHolder.mTextViewTransactionName.setText(transaction.mTransactionOrigPrice);
        viewHolder.mTextViewTransactionPriceGbp.setText(String.format(mAppCompatActivity.getString(R.string.transaction_price_gbp), transaction.mTransactionGbp));
    }

    public ItemModelTransaction getItem(int position)
    {
        return mTransactionsList.get(position);
    }

    @Override
    public int getItemCount()
    {
        return mTransactionsList.size();
    }

}

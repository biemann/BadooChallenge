package biemann.android.badoochallenge.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import biemann.android.badoochallenge.R;

/**
 * Typical Viewholder pattern for RecyclerView
 */
public class ViewHolderProducts extends RecyclerView.ViewHolder
{
    public LinearLayout mLinearLayoutContainer;
    public TextView mTextViewProductName;
    public TextView mTextViewTransactionCount;

    public ViewHolderProducts(View itemView)
    {
        super(itemView);
        mLinearLayoutContainer = (LinearLayout) itemView.findViewById(R.id.product_layout_container);
        mTextViewProductName = (TextView) itemView.findViewById(R.id.product_name);
        mTextViewTransactionCount = (TextView) itemView.findViewById(R.id.product_transaction_count);
    }

    @Override
    public String toString()
    {
        return super.toString() + " '" + mTextViewProductName.getText() + "'";
    }
}

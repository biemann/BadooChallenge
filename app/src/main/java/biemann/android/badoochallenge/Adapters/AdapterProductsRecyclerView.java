package biemann.android.badoochallenge.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import biemann.android.badoochallenge.ActivityTransactions;
import biemann.android.badoochallenge.Models.ItemModelProduct;
import biemann.android.badoochallenge.R;


public class AdapterProductsRecyclerView extends RecyclerView.Adapter<ViewHolderProducts>
{
    private AppCompatActivity mAppCompatActivity;
    ArrayList<ItemModelProduct> mProductsList;

    // Constructor
    public AdapterProductsRecyclerView(AppCompatActivity compatActivity, ArrayList<ItemModelProduct> productsList)
    {
        mAppCompatActivity = compatActivity;
        mProductsList = productsList;
    }

    public void setNewData(ArrayList<ItemModelProduct> productsList)
    {
        mProductsList = productsList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderProducts onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View inflatedView = LayoutInflater.from(mAppCompatActivity).inflate(R.layout.row_product_item, viewGroup, false);
        return new ViewHolderProducts(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolderProducts viewHolder, int position)
    {
        final ItemModelProduct product = getItem(position);
        viewHolder.mTextViewProductName.setText(product.mProductName);
        viewHolder.mTextViewTransactionCount.setText(String.format(mAppCompatActivity.getString(R.string.transaction_count), product.mTransactionCount));
        viewHolder.mLinearLayoutContainer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Context context = v.getContext();
                Intent intent = new Intent(context, ActivityTransactions.class);
                intent.putExtra(ActivityTransactions.EXTRA_PROD_TAG, product.mProductName);
                context.startActivity(intent);
            }
        });
    }

    public ItemModelProduct getItem(int position)
    {
        return mProductsList.get(position);
    }

    @Override
    public int getItemCount()
    {
        return mProductsList.size();
    }

}

package biemann.android.badoochallenge;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;
import java.util.List;

import biemann.android.badoochallenge.Adapters.AdapterTransactionsRecyclerView;
import biemann.android.badoochallenge.Models.ItemModelTransaction;
import biemann.android.badoochallenge.Models.JsonModelRate;
import biemann.android.badoochallenge.Models.JsonModelTransaction;

public class ActivityTransactions extends AppCompatActivity
{
    // Constants
    static public final String EXTRA_PROD_TAG = "EXTRA_PROD_TAG";
    private final String NATIVE_CURRENCY = "GBP";

    // View references
    private RecyclerView mRecyclerView;
    private TextView mTextViewTotalPriceGbp;

    // Other Member Variables
    private ApplicationMain mApplicationMain;
    private AdapterTransactionsRecyclerView mAdapterTransactionsRecyclerView;
    private static ArrayList<ItemModelTransaction> mListTransactions;
    private double mTransactionsTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        // Get passed-in Extras
        String strTransactionSku = getIntent().getStringExtra(EXTRA_PROD_TAG);
        if (strTransactionSku == null) strTransactionSku = getString(R.string.error_missing_transaction);

        // Customize Toolbar title
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(String.format(getString(R.string.title_activity_transactions), strTransactionSku));
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // references
        mApplicationMain = (ApplicationMain)getApplication();
        mRecyclerView = (RecyclerView) findViewById(R.id.productsRecyclerView);
        mTextViewTotalPriceGbp = (TextView) findViewById(R.id.transactions_total);

        // populate views with data
        if (mApplicationMain.mListTransactions != null && mApplicationMain.uniqueRateSet != null)
        {
            // generate graph from transaction currencies
            DirectedGraph<String, DefaultEdge> directedGraph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
            for (String currency : mApplicationMain.uniqueRateSet)
            {
                directedGraph.addVertex(currency);
            }
            for (JsonModelRate rate : mApplicationMain.mListRates)
            {
                directedGraph.addEdge(rate.mFrom, rate.mTo);
            }

            // generate sequential data for use with the UI based on ApplicationMain.mListTransactions
            mListTransactions = new ArrayList<ItemModelTransaction>();
            for (JsonModelTransaction transaction : mApplicationMain.mListTransactions)
            {
                if (transaction.mSku.equals(strTransactionSku))
                {
                    ItemModelTransaction item = new ItemModelTransaction();
                    item.mTransactionOrigPrice = transaction.mCurrency + String.format(" %.2f", transaction.mAmount);
                    if (NATIVE_CURRENCY.equals(transaction.mCurrency))
                    {
                        item.mTransactionGbp = transaction.mAmount;
                        mTransactionsTotal += transaction.mAmount;
                    }
                    else
                    {
                        Double dConvertedValue = Double.valueOf(transaction.mAmount);
                        List<DefaultEdge> path = DijkstraShortestPath.findPathBetween(directedGraph, transaction.mCurrency, NATIVE_CURRENCY);
                        for(DefaultEdge step : path)
                        {
                            // perform the currency conversion of this step
                            final String strFrom = directedGraph.getEdgeSource(step);
                            final String strTo = directedGraph.getEdgeTarget(step);
                            final float rate = mApplicationMain.getRate(strFrom, strTo);
                            dConvertedValue *= rate;
                        }
                        item.mTransactionGbp = new Float(dConvertedValue);
                        mTransactionsTotal += dConvertedValue;
                    }
                    mListTransactions.add(item);
                }
            }

            // make the list look prettier with dividers and add the adapter
            mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));//do this before mRecyclerView.setAdapter()
            mAdapterTransactionsRecyclerView = new AdapterTransactionsRecyclerView(this, mListTransactions);
            mRecyclerView.setAdapter(mAdapterTransactionsRecyclerView);

            // Show the total value of all transactions
            mTextViewTotalPriceGbp.setText(String.format(getString(R.string.transactions_total), mTransactionsTotal));
        }
        else
        {
            // show message to user that there's no data
            // Note: there's no OK button since the app has to be relaunched
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.error_title_missing_data);
            alertDialogBuilder.setMessage(R.string.error_msg_missing_data);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }
}

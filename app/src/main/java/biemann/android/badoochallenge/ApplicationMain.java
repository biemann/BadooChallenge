package biemann.android.badoochallenge;

import android.app.Application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import biemann.android.badoochallenge.Models.JsonModelRate;
import biemann.android.badoochallenge.Models.JsonModelTransaction;
import biemann.android.badoochallenge.Utilities.JsonDataReader;

public class ApplicationMain extends Application
{
    // Member Variables
    public ArrayList<JsonModelRate> mListRates;// basic list of objects based on JSON array
    public ArrayList<JsonModelTransaction> mListTransactions;// basic list of objects based on JSON array
    public Map<String, Integer> mProductsSortedMap;
    public Set<String> uniqueRateSet;

    // NOTE: once this callback is triggered, we have a context
    @Override
    public void onCreate()
    {
        super.onCreate();

        JsonDataReader jsonDataReader = new JsonDataReader(this);
        mListRates = jsonDataReader.getJsonRateListFromResource();
        mListTransactions = jsonDataReader.getJsonTransactionListFromResource();

        // generate a unique list of currencies
        uniqueRateSet = new HashSet<String>();
        for (JsonModelRate rate : mListRates) {
            uniqueRateSet.add(rate.mFrom);
        }

        // generate a count of each product in the list of transactions
        if (mListTransactions != null)
        {
            Map<String, Integer> mapProductCount = new HashMap<String, Integer>();
            for (JsonModelTransaction transaction : mListTransactions)
            {
                Integer count = mapProductCount.get(transaction.mSku);
                mapProductCount.put(transaction.mSku, (count == null) ? 1 : count + 1);
            }

            // based on the hashmap, generate sorted list of products and the frequency of their SKU
            mProductsSortedMap = new TreeMap<String, Integer>(mapProductCount);
        }
    }

    /**
     * finds the conversion rate in the list of  mListRates
     * @param from String identifier, i.e. USD
     * @param to String identifier, i.e. GBP
     * @return null on error (if to and from are not found)
     */
    public Float getRate(String from, String to)
    {
        for (JsonModelRate rate:mListRates)
        {
            if (rate.mFrom.equals(from) && rate.mTo.equals(to))
            {
                return rate.mRate;
            }
        }

        return null;
    }


}

package biemann.android.badoochallenge.Models;

import com.google.gson.annotations.SerializedName;

public class JsonModelTransaction
{
    @SerializedName("amount")
    public Float mAmount;

    @SerializedName("sku")
    public String mSku;

    @SerializedName("currency")
    public String mCurrency;
}

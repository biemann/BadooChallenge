package biemann.android.badoochallenge.Models;

import com.google.gson.annotations.SerializedName;

public class JsonModelRate
{
    @SerializedName("from")
    public String mFrom;

    @SerializedName("to")
    public String mTo;

    @SerializedName("rate")
    public Float mRate;
}

package biemann.android.badoochallenge.Utilities;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

import java.io.InputStream;
import java.util.ArrayList;

import biemann.android.badoochallenge.Models.JsonModelRate;
import biemann.android.badoochallenge.Models.JsonModelTransaction;
import biemann.android.badoochallenge.R;

/**
 * Class that reads the Json and generates java objects
 */
public class JsonDataReader
{
    // Constants
    final private String TAG = getClass().getSimpleName();

    // Member Variables
    private Gson mGson;
    private Context mContext;


    // Constructor
    public JsonDataReader(Context context) {
        mGson = new Gson();
        mContext = context;
    }

    /**
     * read JSON from res/raw/rates.json and return it as a handy ArrayList
     * @return ArrayList<JsonModelRate> or NULL on error
     */
    public ArrayList<JsonModelRate> getJsonRateListFromResource() {

        ArrayList<JsonModelRate> returnData = null;

        final String strJson = getJsonFromResource(R.raw.rates);
        if (strJson != null)
        {
            Type collectionType = new TypeToken<ArrayList<JsonModelRate>>(){}.getType();
            returnData = mGson.fromJson(strJson, collectionType);
        }

        return returnData;
    }

    /**
     * read JSON from res/raw/transactions.json and return it as a handy ArrayList
     * @return ArrayList<JsonModelTransaction> or NULL on error
     */
    public ArrayList<JsonModelTransaction> getJsonTransactionListFromResource() {

        ArrayList<JsonModelTransaction> returnData = null;

        final String strJson = getJsonFromResource(R.raw.transactions);
        if (strJson != null)
        {
            Type collectionType = new TypeToken<ArrayList<JsonModelTransaction>>(){}.getType();
            returnData = mGson.fromJson(strJson, collectionType);
        }

        return returnData;
    }


    /**
     * returns the JSON string from res/raw/resource_id
     * @param resource_id identifier of data file
     * @return
     */
    private String getJsonFromResource(Integer resource_id)
    {
        String strJson = null;

        try {
            InputStream is = mContext.getResources().openRawResource(resource_id);
            byte[] buffer = new byte[is.available()];
            while (is.read(buffer) != -1);
            strJson = new String(buffer);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return strJson;
    }

}

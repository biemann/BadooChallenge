package biemann.android.badoochallenge;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.Map;

import biemann.android.badoochallenge.Adapters.AdapterProductsRecyclerView;
import biemann.android.badoochallenge.Models.ItemModelProduct;

public class ActivityMain extends AppCompatActivity
{
    // View references
    private RecyclerView mRecyclerView;

    // Other Member Variables
    private ApplicationMain mApplicationMain;
    private AdapterProductsRecyclerView mAdapterProductsRecyclerView;
    private static ArrayList<ItemModelProduct> mListProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_main);
        setSupportActionBar(toolbar);

        // references
        mApplicationMain = (ApplicationMain)getApplication();
        mRecyclerView = (RecyclerView) findViewById(R.id.productsRecyclerView);

        // populate views with data
        if (mApplicationMain.mProductsSortedMap != null)
        {
            // generate sequential data for use with the UI based on ApplicationMain.mProductsSortedMap
            mListProducts = new ArrayList<ItemModelProduct>();
            for (Map.Entry<String, Integer> entry : mApplicationMain.mProductsSortedMap.entrySet())
            {
                ItemModelProduct product = new ItemModelProduct();
                product.mProductName = entry.getKey();
                product.mTransactionCount = String.valueOf(entry.getValue());
                mListProducts.add(product);
            }

            // make the list look prettier with dividers and add the adapter
            mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));//do this before mRecyclerView.setAdapter()
            mAdapterProductsRecyclerView = new AdapterProductsRecyclerView(ActivityMain.this, mListProducts);
            mRecyclerView.setAdapter(mAdapterProductsRecyclerView);
        }
        else
        {
            // show message to user that there's no data
            // Note: there's no OK button since the app has to be relaunched
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.error_title_missing_data);
            alertDialogBuilder.setMessage(R.string.error_msg_missing_data);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }
}
